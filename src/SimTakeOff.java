

public class SimTakeOff implements TakeOff {

	int id; 
	int arrivalPathTime=-1; 
	int startTakeOffTime=-1;
	int finishTakeOffTime=-1;
	
	public SimTakeOff(int id,int t) {
		this.id=id;
		arrivalPathTime = t;
		
		System.out.println(this + " arrived ground at time " + t);
	}
	
	public int getStartTakeOffTime() {
		return startTakeOffTime;
	}
	
	public int getFinishTakeOffTime() {
		return finishTakeOffTime;
	}
	
	public void setStartTakeOffTime(int t) {
		startTakeOffTime = t;
	}
	
	public void setFinishProcessTakeOffTime(int t) {
		finishTakeOffTime = t;
	}
	
	public String toString() {
		return "Airplane " + id;
	}
}

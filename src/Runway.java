
public interface Runway {

	public int getMeanProcessTime();
	public int getFinishProcessTime();
	public boolean isIdleLanding();
	public boolean isIdleTakeOff();
	public void startProcessLanding (Landing landing, int t);
	public void startProcessTakeOff (TakeOff takeoff, int t);
	public void finishProcessLanding (int t);
	public void finishProcessTakeOff (int t);
}

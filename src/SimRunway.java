

public class SimRunway  implements Runway {
	
	private Landing landing;
	private TakeOff takeoff;
	private int id; 
	//private int meanLandingTime;
	//private int meanTakeOffTime;
	private int meanProcessTime;
	//private int finishLandingTime=-1;
	///private int finishTakeOffTime=-1;
	private int finishProcessTime =-1;
	private java.util.Random random;
	
	public SimRunway (int id, int meanProcessTime) {
		this.id = id;
		this.meanProcessTime = meanProcessTime;
		this.random = new ExponentialRandom(meanProcessTime);
	}
	

	public int getMeanProcessTime() {
		return meanProcessTime;
	}
	
	public int getFinishProcessTime() {
		return finishProcessTime;
	}
	

	public boolean isIdleLanding() {
		return landing == null;
	}
	
	public boolean isIdleTakeOff() {
		return takeoff == null;
	}
	
	public void startProcessLanding (Landing landing, int t) {
		this.landing = landing;
		this.landing.setStartLandingTime(t);
		this.finishProcessTime = t + random.nextInt();
		
		System.out.println(landing + " started landing at " + this
				+ " at time " + t + " and will finish at time " + finishProcessTime);
	}
	
	public void startProcessTakeOff  (TakeOff takeoff, int t) {
		this.takeoff = takeoff;
		this.takeoff.setStartTakeOffTime(t);
		this.finishProcessTime = t + random.nextInt();
		
		System.out.println(takeoff + " started take off at " + this
				+ " at time " + t + " and will finish at time " + finishProcessTime);
	}
	
	public void finishProcessLanding (int t) {
		landing.setFinishProcessLandingTime(t);
		
		System.out.println(landing + " landed " + this
				+ " at time " + t);
		
		landing = null;
	}
	
	public void finishProcessTakeOff (int t) {
		takeoff.setFinishProcessTakeOffTime(t);
		
		System.out.println(takeoff + " successfull take off " + this
				+ " at time " + t);
		
		takeoff = null;
	}
	@Override
	public String toString() {
		String s="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    return "Runway Path " + s.charAt(id);  
	}

}


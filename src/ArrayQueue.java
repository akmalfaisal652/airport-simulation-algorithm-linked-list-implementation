

public class ArrayQueue implements Queue {

	private Object[] objQ;
	private int frontIndex;
	private int backIndex;
	private int size;
	
	public ArrayQueue (int capacity) {
		objQ = new Object[capacity];
		frontIndex = 0;
		backIndex = capacity - 1;
		size = 0;
	}
	
	public void enqueue(Object object) {
		ensureCapacity();
		backIndex = (backIndex + 1) % objQ.length;
		objQ[backIndex] = object;
		size++;
	}
	
	public Object dequeue() {
		Object front;
		
		if(isEmpty()) throw new
			IllegalStateException("queue is empty");
		
		front = objQ[frontIndex];
		frontIndex = (frontIndex + 1) % objQ.length;
		size--;
		
		return front;
	}
	
	public boolean isFull() {
		return size == objQ.length;
	}
	
	private void ensureCapacity() {
		if(isFull()) {
			Object[] oldQ = objQ;
			
			objQ = new Object[2 * oldQ.length];
			
			for(int i = 0; i< oldQ.length;i++) {
				objQ[i] = oldQ[frontIndex];
				frontIndex = (frontIndex + 1) % oldQ.length;
			}
			
			frontIndex = 0;
			backIndex = oldQ.length - 1;
		}
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	public Object getFront() {
		Object front = null;
		
		if(isEmpty()) throw new 
			IllegalStateException("queue is empty");
		
		front = objQ[frontIndex];
		
		return front;
	}
	
	public int size() {
		return size;
	}
}

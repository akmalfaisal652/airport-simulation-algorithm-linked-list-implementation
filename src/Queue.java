public interface Queue {

	public void enqueue(Object object);
	public Object getFront();
	public Object dequeue();
	public int size();
	public boolean isEmpty();
}

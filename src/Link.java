public interface Link {

	public Object getFirst();
	public Object getLast();
	public void addFirst(Object o);
	public void addLast(Object o);
	public void add(int index, Object o);
	public Object removeFirst();
	public Object removeLast();
	public Object remove(int index);
	public String toString();
	public void clear();
	public boolean contains(Object o);
	public Object get(int index);
	public int indexOf(Object o);
	public int lastIndexOf(Object o);
	public Object set(int index, Object o);

}


public class SimLanding implements Landing {

	int id; 
	int arrivalPathTime=-1; 
	int startLandingTime=-1;
	int finishLandingTime=-1;
	
	public SimLanding(int id,int t) {
		this.id=id;
		arrivalPathTime = t;
		
		System.out.println(this + " arrived air at time " + t);
	}
	
	public int getStartLandingTime() {
		return startLandingTime;
	}
	
	public int getFinishLandingTime() {
		return finishLandingTime;
	}
	
	public void setStartLandingTime(int t) {
		startLandingTime = t;
	}
	
	public void  setFinishProcessLandingTime(int t) {
		finishLandingTime = t;
	}
	
	public String toString() {
		return "Airplane " + id;
	}
}

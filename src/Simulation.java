

public class Simulation {

	static int numRunwayPath;
	static int numAirplaneLanding;
	static int numAirplaneTakeOff;
	static int meanProcessTime;
	static int meanInterarrivalTime;
	static Runway [] runways;
	static Landing [] ariplaneLanding;
	static TakeOff [] ariplaneTakeOff;
	static Queue queueLanding = new ArrayQueue(numAirplaneLanding);
	static Queue queueTakeOff = new ArrayQueue(numAirplaneTakeOff);

	PriorityQueue<queueLanding> landingList = new PriorityQueue<queueLanding>(meanProcessTime);
        LinkedList<queueTakeOff> takeoffList = new LinkedList<queueTakeOff>();

	static java.util.Random randomArrival;
	static java.util.Random randomProcess;
	
	public static void main(String [] args) {
		init(args);
	}
	
	static void init (String[] args) {
		if(args.length<10) {
			System.out.println("Usage : java Simulation <numRunwaypath> "
					+ "<numAirplaneLanding>  <numAirplaneTakeOff> <meanProcessTime> <meanInterarrivalTime>");
			System.out.println("eg.: java Simulation 1 5 5 2 1");
		}
		
		numRunwayPath  =Integer.parseInt("1");
		numAirplaneLanding = Integer.parseInt("5");
		numAirplaneTakeOff = Integer.parseInt("5");
		meanProcessTime  =Integer.parseInt("2");
		meanInterarrivalTime = Integer.parseInt("1");
		runways = new Runway[numRunwayPath];
		airplaneLanding = new Landing [numAirplaneLanding];
		airplaneTakeOff= new TakeOff [numAirplaneTakeOff];
		randomProcess = new ExponentialRandom (meanProcessTime);
		randomArrival = new ExponentialRandom (meanInterarrivalTime);
		queueLanding = new ArrayQueue(numAirplaneLanding);
		queueTakeOff = new ArrayQueue(numAirplaneTakeOff);
		
		for(int j=0;j<numRunwayPath;j++) {
			runways[j] = new SimRunway (j,randomProcess.nextInt());
			System.out.println("Number of Runway Path = " + numRunwayPath);
			System.out.println("Number of Airplane in the air = " + numAirplaneLanding);
			System.out.println("Number of Airplane on the ground = " + numAirplaneTakeOff);
			System.out.println("Mean Process time = " + meanProcessTime);
			System.out.println("Mean interarrival time = " + meanInterarrivalTime);
			
			for (j=0; j<numRunwayPath; j++)
				System.out.println("Mean Process time for " + runways[j]
						+ " = " + runways[j].getMeanProcessTime());
			
			// Queue Airplane for landing 
			for(int t = 0,  i=0;i<numAirplaneLanding  ;t++) { 
				if(t==meanInterarrivalTime) {
					Landing landing = airplaneLanding[i++] = new SimLanding(i,t);
					queueLanding.enqueue(landing);
					meanInterarrivalTime = t  + randomArrival.nextInt();
				}
				
				for (j =0;j<numRunwayPath;j++) {
					Runway runway = runways[j];
						if(t==runway.getFinishProcessTime()) 
							runway.finishProcessLanding(t);
						if(runway.isIdleLanding() && !queueLanding.isEmpty()) {
							Landing landing = (SimLanding)queueLanding.dequeue();
							runway.startProcessLanding(landing, t);
						}
				} 
				
			
			} 
			
			// Queue Airplane for take off
			for(int t = 0,  i=0;i<numAirplaneTakeOff  ;t++) { 
				if(t==meanInterarrivalTime) {
					TakeOff takeoff = airplaneTakeOff[i++] = new SimTakeOff(i,t);
					queueLanding.enqueue(takeoff);
					meanInterarrivalTime = t  + randomArrival.nextInt();
				}
				
				for (j =0;j<numRunwayPath;j++) {
					Runway runway = runways[j];
						if(t==runway.getFinishProcessTime()) 
							runway.finishProcessLanding(t);
						if(runway.isIdleTakeOff() && !queueTakeOff.isEmpty()) {
							TakeOff takeoff = (SimTakeOff)queueTakeOff.dequeue();
							runway.startProcessTakeOff(takeoff, t);
						}
				} 
				
			
			}
		}
	}
}

